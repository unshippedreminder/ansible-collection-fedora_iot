Wireguard Server
=========

Sets up a Wireguard server and configure it with a provided config file.
Config is templated from 'templates/wg0.conf.j2' and secrets are configured from a vault.

Requirements
------------

- `rpm-ostree`

Role Variables
--------------

```
wireguard:
  server:
    priv: ""
    pub: ""
    address: ""
    port: ""
  peers:
    - name: ""
      #priv: ""
      pub: ""
      allowed_ips: ""
    - name: ""
      #priv: ""
      pub: ""
      allowed_ips: ""
```

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

Copyright (C) 2023  UnshippedReminder

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
