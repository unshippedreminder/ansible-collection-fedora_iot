Unprivileged User
=========

Configures an unprivileged user to securely interact with the system.

1. The user is added to the 'wheel' group. 
  - Note that wheels sudo privileges are configured by 'unshippedreminder.fedora_iot.common'.
2. 'unprivileged_user_ssh_keys' are added to the users authorized_keys.
3. Installs:
  - `rcm`
    - Dotfiles management.
  - `git`
    - For downloading dotfiles.
  - `neovim`
  - `toolbox`
  - `audit`
  - `acl`
    - Allows Ansible to run commands as another user than the (unprivileged) 'ansible' user.
  - `starship`
    - Cross shell prompt.
4. Installs Dotfiles from `unprivileged_user_dotfiles_repo`
5. Disables Zezere

Requirements
------------

- `github3.py`
  - Must be present on the Controller/the host from which your playbook was run.

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

- `unprivileged_user_ssh_keys`
  - SSH public key. (Preferably in a vault)
- `unprivileged_user_shell`
  - Full path to preferred shell. (defaults to `/usr/bin/bash`)
- `unprivileged_user_dotfiles_repo`
  - Link to your *server dotfiles* repository.
  - Will be soft linked to the home directory of 'user'.

Dependencies
------------

- `rpm-ostree`

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

Copyright (C) 2023  UnshippedReminder

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
