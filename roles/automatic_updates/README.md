Automatic Updates
=========

Sets up *automatic updates*.

Only supports `rpm-ostree`.

The `rpm-ostree` task can be configured to reboot at defined intervals via the 'automatic_updates_reboot' variable.

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

- 'automatic_updates_reboot'
  - Will restart the host at defined times if set to one of the below values.
  - If undefined, this will be disabled.
  - Allowed values:
    - 'daily'
      - Will restart every day between 04:00 and 06:00
    - 'weekly'
      - Will restart every week, on Monday, between 04:00 and 06:00
    - 'monthly'
      - Will restart every month, on the 1st, between 04:00 and 06:00

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

Copyright (C) 2023  UnshippedReminder

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
