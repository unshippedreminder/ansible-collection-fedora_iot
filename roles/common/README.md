Common
=========

Configures basic settings, common to all machines:
- Hostname (from Inventory).
- Timezone. [common_tz]
- Installs qemu-agent if running under KVM.

Locks down the system:
- Ensures SELinux and FirewallD are active.
- Sets the shell of 'root' to `/usr/sbin/nologin`.
- Disables PasswordAuthentication, PermitRootLogin and X11Forwarding.
- Configures *wheel*s sudo 'nopassword' priviliges. [common_sec_sudo_passwordless] \(Defaults to 'false')

Requirements
------------

Only works on Fedora IOT

Role Variables
--------------

- common_sec_sudo_passwordless \(Defaults to 'false')
  - Allows members of group 'wheel' to use sudo without providing a password.

License
-------

Copyright (C) 2023  UnshippedReminder

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
