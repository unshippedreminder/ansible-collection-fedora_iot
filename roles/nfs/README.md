Role Name
=========

A brief description of the role goes here.

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

```yaml
nfs_exclusive: true
nfs_allow_insecure: false
nfs_exports_server: 10.10.10.2
nfs_exports:
  - path: /srv/media
    hosts:
      - name: 10.10.10.3
        options:
          - ro
      - name: 10.10.10.17
        options:
          - rw
  - path: /srv/monitoring
    hosts:
      - name: 10.10.11.1
      - name: 10.11.10.1
nfs_clients:
  - client: <ansible_hostname of client>
    server: fiot0.lan
    mount: /srv/monitoring
    path: /mnt/monitoring
    options:
      - rw
  - client: <ansible_hostname of client>
    server: fiot0.lan
    mount: /srv/media
    path: /mnt/media
    options:
      - rw
```

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
